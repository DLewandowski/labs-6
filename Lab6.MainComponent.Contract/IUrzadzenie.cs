﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.MainComponent.Contract
{
    public interface IUrzadzenie
    {
        void Funkcjonalnosc1(string message);
        void Funkcjonalnosc2(string message);
        void Extended(string message);
    }
}
