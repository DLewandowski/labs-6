﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.ControlPanel.Contract;
using System.Windows;
using Lab6.MainComponent.Contract;

namespace Lab6.ControlPanel.Implementation
{
    public class ControlPanel:IControlPanel
    {
        private Window Panel;

        public ControlPanel(IUrzadzenie u)
        {
            Panel = new GUI(u);
        }

        public Window Window
        {
            get { return Panel; }
        }
    }
}
