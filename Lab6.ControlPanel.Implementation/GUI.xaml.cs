﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Lab6.MainComponent.Contract;

namespace Lab6.ControlPanel.Implementation
{
    /// <summary>
    /// Interaction logic for GUI.xaml
    /// </summary>
    partial class GUI : Window
    {
        private IUrzadzenie _urzadzenie;

        public GUI(IUrzadzenie u)
        {
            _urzadzenie = u;
            InitializeComponent();
         //   _winda.DoDolu(Assembly.GetAssembly(winda.GetType()).GetName().Version.ToString());
          //  _winda.DoDolu(Assembly.GetExecutingAssembly().GetName().Version.ToString());
            if (Assembly.GetAssembly(_urzadzenie.GetType()).GetName().Version.Major < 2)
            {
                //this.btn_ex.IsEnabled = false;
                this.btn_ex.Visibility = Visibility.Hidden;
            }
        }

        private void event_f1(object sender, RoutedEventArgs e)
        {
            _urzadzenie.Funkcjonalnosc1("Użyto #1");
        }

        private void event_f2(object sender, RoutedEventArgs e)
        {
            _urzadzenie.Funkcjonalnosc2("Użyto #2");
        }

        private void event_extended(object sender, RoutedEventArgs e)
        {
            var bonus = _urzadzenie.GetType().GetMethod("Extended");
            if (bonus != null)
            {
                object[] tmp = new object[1] { "Tylko w v2.0" };
                bonus.Invoke(_urzadzenie, tmp);
            }
        }
    }
}
