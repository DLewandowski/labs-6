﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.Display.Contract;
using Lab6.MainComponent.Contract;

namespace Lab6.MainComponent.Implementation
{
    public class Urzadzenie : IUrzadzenie
    {
        private IDisplay _display;

        public Urzadzenie(IDisplay display)
        {
            _display = display;
            _display.Text = "";
        }

        public void Funkcjonalnosc1(string message)
        {
            _display.Text = message;
        }

        public void Funkcjonalnosc2(string message)
        {
            _display.Text = message;
        }

        public void Extended(string message)
        {
            _display.Text = message;
        }
    }
}
