﻿using Lab6.ControlPanel.Contract;
using PK.Container;
using System;
using System.Reflection;
using Lab6.MainComponent.Implementation;
using Lab6.MainComponent.Contract;

namespace Lab6.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Func<IContainer> ContainerFactory = () => new Adapter();

        public static Assembly ControlPanelSpec = Assembly.GetAssembly(typeof(IControlPanel));
        public static Assembly ControlPanelImpl = Assembly.GetAssembly(typeof(ControlPanel.Implementation.ControlPanel));

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IUrzadzenie));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(MainComponent.Implementation.Urzadzenie));

        #endregion
    }
}
