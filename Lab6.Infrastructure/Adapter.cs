﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PK.Container;
using Lab6.ControlPanel.Contract;
using Lab6.Display.Contract;
using Microsoft.Practices.Unity;


namespace Lab6.Infrastructure
{
    public class Adapter:IContainer
    {
         public UnityContainer _container = new UnityContainer();

        public void Register<T>(Func<T> provider) where T : class
        {
            Register<T>(provider.Invoke());
        }

        public void Register<T>(T impl) where T : class
        {
            foreach (var _interface in typeof(T).GetInterfaces())
            {
                _container.RegisterInstance(_interface, impl);
            }
        }

        public void Register(Type type)
        {
            _container.RegisterType(type);
        }

        public void Register(System.Reflection.Assembly assembly)
        {
            foreach (var type in assembly.GetTypes())
            {
                if(!type.IsInterface)
                {
                    foreach (var _interface in type.GetInterfaces())
                    {
                        _container.RegisterType(_interface, type);
                    }
                    _container.RegisterType(type);
                }
            }
        }

        public object Resolve(Type type)
        {
            try
            {
                return _container.Resolve(type);
            }
            catch(ResolutionFailedException)
            {
                throw new UnresolvedDependenciesException();
            }
            catch(Exception)
            {
                throw new UnresolvedDependenciesException();
            }
        }

        public T Resolve<T>() where T : class
        {
            try
            {
                return (T)_container.Resolve(typeof(T));
            }
            catch (ResolutionFailedException)
            {
                throw new UnresolvedDependenciesException();
            }
            catch(Exception)
            {
                throw new UnresolvedDependenciesException();
            }
        }
    }
}
